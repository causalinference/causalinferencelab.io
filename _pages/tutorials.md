---
permalink: /tutorials/
title: Tutorials
layout: single

---

We recently gave a tutorial on causal inference and counterfactual reasoning at KDD. Slides are available at <https://causalinference.gitlab.io/kdd-tutorial/>.

This is a broad tutorial covering basics of graphical and potential outcome
frameworks, conditioning and natural experiment-based methods, sensitivity
analyis, and connections to machine learning.  

If you prefer a more breezy, hands-on introduction to key concepts, check out Amit Sharma's tutorial at IC2S2. It includes code to try out the methods for yourself!
<https://github.com/amit-sharma/causal-inference-tutorial>

If you prefer a focused tutorial connecting machine learning to conditioning methods in causal inference, check out David Sontag and Uri Shalit's tutorial at ICML 2016. 
<https://cs.nyu.edu/~shalit/tutorial.html>

 
