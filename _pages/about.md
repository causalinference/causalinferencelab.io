---
permalink: /about/
title: About
layout: single

---
A website for resources on causal inference. 

Compiled by Amit Sharma [@amt_shrma](https://twitter.com/amt_shrma) and Emre Kiciman
[@emrek](https://twitter.com/emrek).

Suggestions welcome!
