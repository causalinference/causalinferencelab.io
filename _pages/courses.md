---
permalink: /courses/
title: Courses
layout: single

---

There are a few good courses to get started on causal inference and their
applications in computing/ML systems. 

* Causality in machine learning by Robert Ness [[Lecture Nodes](https://bookdown.org/robertness/causalml/docs/)] [[Github](https://github.com/robertness/causalML)] 

* Causal inference and learning by Elena Zheleva [[Reading list](https://www.cs.uic.edu/~elena/courses/fall19/cs594cil.html)] 

* Causal data science by Paul Hunermund  [[Course](https://www.udemy.com/course/causal-data-science/)]

And here are some sets of lectures.

* Lectures on causality by Jonas Peters [[Video](https://www.youtube.com/watch?v=zvrcyqcN9Wo)]

* Drawing causal inference from big data -- NAS Sackler Colloquium [[Video](https://www.youtube.com/watch?v=L72E08QsyMc&list=PLGJm1x3XQeK0NgFOX2Z7Wt-P5RU5Zv0Hv)] 

* Causal inference in everyday machine learning by Ferenc Huszar
  [[Video](https://www.youtube.com/watch?v=HOgx_SBBzn0)]

* Causal discovery by Bernhard Scholkopf  [[Video](https://www.youtube.com/watch?v=CTcQlRSnvvM)]




