---
permalink: /code/
title: Code
layout: single

---

**DoWhy: Python Library**

Much like machine learning libraries have done for prediction, “DoWhy” is a Python library that
aims to spark causal thinking and analysis. DoWhy provides a unified interface for causal inference
methods and automatically tests many assumptions, thus making inference accessible to non-experts.

Check out our introductory [blog post](https://www.microsoft.com/en-us/research/blog/dowhy-a-library-for-causal-inference/) on DoWhy

Code: [Github](https://github.com/Microsoft/dowhy/).
Documentation: <https://microsoft.github.io/dowhy/>.

**Other packages we find useful**

Once you have done the hard work of identifying the causal estimand, you can also try out this library by Adam Kelleher that implements non-parametric estimation methods. <https://github.com/akelleh/causality>
 
For causal *discovery*, check out DAGitty, an excellent package that lets you
reason with causal graphs.
<http://www.dagitty.net/> 


   

