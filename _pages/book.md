---
permalink: /book/
title: "Causal Reasoning: Fundamentals and Machine Learning Applications"
layout: single

---
We are writing a book on causal reasoning with an explicit focus on computing systems. We will be posting book chapters here as we complete them. 

We'd love your feedback on the chapters. You can leave a comment on the chapters below or send us an email.


* [Book outline]({% post_url 2019-12-11-Causal Reasoning: Fundamentals and Machine Learning Applications %})
* Chapter 1: [Introduction]({% post_url 2019-12-11-causal-reasoning-book-chapter1 %})
* Chapter 2: [Models and Assumptions]({% post_url 2020-03-20-causal-reasoning-book-chapter2 %})
* Chapter 3: [Identification]({% post_url 2021-03-29-causal-reasoning-book-chapter3 %})
* Chapter 4: [Estimation]({% post_url 2021-04-05-causal-reasoning-book-chapter4 %})
* Chapter 5: [Refutation and Sensitivity Analysis (TODO)]()


## Other great books
For a *casual* introduction to causality: 
* Pearl. "The Book of Why: The New Science of Cause and Effect" \[[Link](https://www.amazon.com/Book-Why-Science-Cause-Effect/dp/046509760X/ref=sr_1_1)\]  

For a general introduction that covers both potential outcome and graphical
model frameworks:
* Morgan, Winship. "Counterfactuals and Causal Inference: Methods and Principles
  for Social Research" \[[Link](https://www.amazon.com/Counterfactuals-Causal-Inference-Principles-Analytical/dp/0521671930)\]

For a technical introduction, accessible to most: 
* Pearl, Glymour, Jewell. “Causal Inference in Statistics: A Primer” \[[Link](https://www.amazon.com/Causal-Inference-Statistics-Judea-Pearl/dp/1119186846/ref=pd_bxgy_img_2)\] 

For an econometric view, with a focus on local identification:
* Angrist, Pischke. "Mastering Metrics: The Path from Cause to Effect" \[[Link](https://www.amazon.com/Mastering-Metrics-Path-Cause-Effect/dp/0691152845)\]

For statistical estimation and design of analysis: 
* Rosenbaum, “Design of Observational Studies”\[[Link](https://www.amazon.com/Design-Observational-Studies-Springer-Statistics/dp/1441912126/ref=sr_1_2)\]

For connections to machine learning: 
* Peters, Janzing, Schoelkopf.  “Elements of Causal Inference: Foundations and Learning Algorithms”\[[Link](https://www.amazon.com/Elements-Causal-Inference-Foundations-Computation-ebook)\]
