---
title: "Book outline---Causal Reasoning: Fundamentals and Machine Learning Applications"
sidebar:
    nav: "book-outline"
---

This book is aimed at students and practitioners familiar with machine learning
(ML) and data science.  Our goal is to provide an accessible introduction to
causal reasoning and its intersections with machine learning, with a particular
focus on the challenges and opportunities brought about by large-scale
computing systems acting as interventions in the world, ranging from online 
recommendation systems to healthcare decision support systems.  We hope to 
provide a practical perspective to working on causal inference problems and
a unified interpretation of methods from varied fields such as statistics, 
econometrics and computer science, drawn from our experience in applying these
methods to online computing systems. 

Throughout, methods and complicated statistical ideas are motivated and 
explained through practical examples in computing systems and their applications.
In addition, we devote a third of the book to discussing machine learning
applications of causal inference in detail, in different settings such as 
recommendation systems, system experimentation, learning from log data, 
generalizing predictive models, and fairness in computing systems.  

Beyond our focus on machine learning applications, we expect that three aspects of
our perspective on causal reasoning will be woven throughout our treatment (pun
not intended) of this material, to help organize our materials and provide what
may be a distinct viewpoint on causal reasoning.  While this book is targeted
primarily for computer scientists, these aspects may also make the book useful 
for learners more broadly:

1. We present a unified view of causality frameworks, including the two major 
frameworks from statistics (Potential outcomes framework) and computer science
(Bayesian graphical models) which are often not presented together. We present 
how these are compatible frameworks that have different strengths, are 
appropriate for different stages of a causal reasoning pipeline, and provide
practical advice on how to combine them in a causal inference analysis. In 
addition, by introducing causal inference through the core concepts of
interventions and counterfactuals, we introduce causal inference
methods from a “first-principles” approach, creating a clear taxonomy of 
back-door and natural experiment methods and highlighting similarities between
various methodologies. 

2. We make an explicit distinction between identification (causal) and
estimation (statistical) methods. While this distinction is fundamental to
causal reasoning, it is overlooked in many texts on causal inference, preventing
readers from understanding the distinction from statistical methods and sources
of error in their causal estimate. Through this distinction, we make a natural
connection to machine learning: ML can be useful for all statistical parts of
causal reasoning, but it is not useful for identification, which follows from
causal assumptions, whether implicit or explicit. Throughout the book, we discuss
how machine learning can enrich estimation methods by allowing non-parametric
estimation, and how causal reasoning can be useful to make ML methods more
robust to environmental changes.  

3. Finally, we discuss the criticality of assumptions in any causal analysis
and present practical ways to test the robustness of a causal estimate to 
violation of its assumptions. We refer to this exercise as “refuting” the 
estimate, in a similar way to how refutation of scientific theories is a common
way to test their relevance. Based on our experience, we present different ways
to test assumptions, check robustness and conduct sensitivity analysis for any
obtained estimate.     

Throughout, we will include code examples using [DoWhy](https://github.com/microsoft/dowhy), a Python library for causal inference.  

---

The current outline of our book is as follows:

### PART I. Introduction to Causal Reasoning {#choutline-part1}

Part I. of our book describes the four steps of causal inference and the intuitions and core technologies underlying each step.  Chapter 2 covers modeling of causal assumptions using causal graphs.  Chapter 3 presents the analytical methods for identification, including how Do-Calculus and additional assumptions can be used to derive common identification strategies such as the adjustment formula and instrumental variables.  Chapter 4 presents a variety of statistical estimation methods and their practical considerations, including methods based on balance, weighting, outcome-modeling, and thresholds.  Chapter 5 discusses approaches for sensitivity analysis, validation of assumptions, and other evaluation of causal effects.

Chapter 1. Introduction

Chapter 2. Causal Models, Assumptions

Chapter 3. Identification

Chapter 4. Causal Estimation

Chapter 5. Refutations, Validations, and Sensitivity Analyses


### Part II. Causal Machine Learning {#choutline-part2}

In the second half of this book, we focus on the connections between causal reasoning and its connections to core machine learning tasks (Chapter 6).
We build on the core intuitions and components of causal inference introduced in Part I and show how they can be recombined to address critical challenges in experimentation and reinforcement learning (Chapter 7);
learning and off-policy evaluation from biased logs and other observational data (Chapter 8); robustness, generalization and domain adaptation of machine learning models (Chapter 9), and interpretability, explainability and bias in machine learning (Chapter 10).


Chapter 6. Connections between Causality and Machine Learning

Chapter 7. Experimentation and Reinforcement Learning

Chapter 8. Learning from Logged Data

Chapter 9. Generalization in Classification and Prediction

Chapter 10. Machine Learning Explanations and Bias

---


We have posting Chapter 1-4 of our book as of April 2021, and will be releasing with
new chapters regularly.  Our texts will often be rough, especially on
their initial posting, and we expect they will see substantial change throughout
the writing process.  We appreciate in advance your patience with
our errors and mistakes, as well as your comments and feedback throughout.
